# Internal security groups rules need reviewing to allow exec/logs etc to work between nodes
# Temporarily allowed access to all.
# Also it failed to let HTTP traffic through ingress

resource "aws_security_group" "allow_all" {
  name        = "allow_all_rke2-test"
  description = "Allow all traffic"
  vpc_id      = var.vpc

  ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_http_from_internet" {
  name        = "allow_http_from_internet"
  description = "Allow all HTTP(s) Traffic from the Internet"
  vpc_id      = var.vpc

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_ssh_from_internet" {
  name        = "allow_ssh_from_internet"
  description = "Allow SSH Traffic from the Internet"
  vpc_id      = var.vpc

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_internet_access" {
  name        = "allow_internet_access"
  description = "Allow Internet Access"
  vpc_id      = var.vpc

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_k8s_api_access" {
  name        = "allow_k8s_api_access"
  description = "Allow Kubernetes and Rancher API Access"
  vpc_id      = var.vpc

  ingress {
    from_port   = 6443
    to_port     = 6443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }


  ingress {
    from_port   = 9345
    to_port     = 9345
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

}

resource "aws_route53_zone" "main" {
  name = var.domainname
  vpc {
    vpc_id = var.vpc
  }
  lifecycle {
    ignore_changes = [vpc]
  }
}
