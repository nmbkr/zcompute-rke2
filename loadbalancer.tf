resource "aws_alb_target_group" "http-instances" {
  name               =   "RKE2-HTTP"
  port               =   80
  protocol           =   "TCP"
  vpc_id             =   var.vpc
}

resource "aws_alb_target_group" "https-instances" {
  name               =   "RKE2-HTTPS"
  port               =   443
  protocol           =   "TCP"
  vpc_id             =   var.vpc
}

resource "aws_alb_target_group" "k8s-api-instances" {
  name               =   "K8S-API"
  port               =   6443
  protocol           =   "TCP"
  vpc_id             =   var.vpc
}

resource "aws_alb_target_group" "rancher-api-instances" {
  name               =   "RANCHER-API"
  port               =   9345
  protocol           =   "TCP"
  vpc_id             =   var.vpc
}

resource "aws_alb_target_group_attachment" "attach_rke2_http" {
  target_group_arn   =   aws_alb_target_group.http-instances.arn
  target_id          =   element(aws_instance.worker.*.id, count.index)
  port               =   80
  count              =   var.quantity-worker
}

resource "aws_alb_target_group_attachment" "attach_rke2_https" {
  target_group_arn   =   aws_alb_target_group.https-instances.arn
  target_id          =   element(aws_instance.worker.*.id, count.index)
  port               =   443
  count              =   var.quantity-worker
}

resource "aws_alb_target_group_attachment" "attach_rke2_k8s_api" {
  target_group_arn   =   aws_alb_target_group.k8s-api-instances.arn
  target_id          =   element(aws_instance.manager.*.id, count.index)
  port               =   6443
  count              =   var.quantity-manager
  #count              =   1
}

resource "aws_alb_target_group_attachment" "attach_rke2_rancher_api" {
  target_group_arn   =   aws_alb_target_group.rancher-api-instances.arn
  target_id          =   element(aws_instance.manager.*.id, count.index)
  port               =   9345
  count              =   var.quantity-manager
}

resource "aws_alb" "alb" {
  name               =   "rke2-loadbalancer"
  internal           =   false
  load_balancer_type =   "network"
  security_groups    =   [aws_security_group.allow_all.id]
  subnet_mapping {
    subnet_id        =   var.subnet
  }
}

resource "aws_alb_listener" "rke2-nlb-http" {
  load_balancer_arn  =   aws_alb.alb.arn
  port               =   "80"
  protocol           =   "TCP"

  default_action {
  type               =   "forward"
  target_group_arn   =   aws_alb_target_group.http-instances.arn
  }
}

resource "aws_alb_listener" "rke2-nlb-https" {
  load_balancer_arn  =   aws_alb.alb.arn
  port               =   "443"
  protocol           =   "TCP"

  default_action {
  type               =   "forward"
  target_group_arn   =   aws_alb_target_group.https-instances.arn
  }
}

resource "aws_alb_listener" "rke2-nlb-k8s-api" {
  load_balancer_arn  =   aws_alb.alb.arn
  port               =   "6443"
  protocol           =   "TCP"

  default_action {
  type               =   "forward"
  target_group_arn   =   aws_alb_target_group.k8s-api-instances.arn
  }
}

resource "aws_alb_listener" "rke2-nlb-rancher-api" {
  load_balancer_arn  =   aws_alb.alb.arn
  port               =   "9345"
  protocol           =   "TCP"

  default_action {
  type               =   "forward"
  target_group_arn   =   aws_alb_target_group.rancher-api-instances.arn
  }
}